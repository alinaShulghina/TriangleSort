package com.softserve.edu.triangle;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by alin- on 12.12.2017.
 */
public class TriangleTest {

    private Triangle triangle;
    private final double DELTA = 0.0001;
    private String triangleName = "Tril";
    private double sideA;
    private double sideB;
    private double sideC;

    @Test(expected = IllegalArgumentException.class)
    public void calculateSquareForNotExistingTriangle() {
        sideA = 3.4;
        sideB = 7.8;
        sideC = 2.3;

        triangle = new Triangle(triangleName, sideA, sideB, sideC);
    }

    @Test(expected = IllegalArgumentException.class)
    public void calculateSquareForNegativeValues() {
        sideA = -1;
        sideB = -4.5;
        sideC = -6.7;

        triangle = new Triangle(triangleName, sideA, sideB, sideC);
    }


    @Test
    public void calculateSquareForDoubleValues() {
        double result;
        sideA = 4.5;
        sideB = 9.1;
        sideC = 6.78;

        triangle = new Triangle(triangleName, sideA, sideB, sideC);

        result = 14.6802;

        assertEquals(result, triangle.getSquare(), DELTA);
    }

    @Test(expected = IllegalArgumentException.class)
    public void calculateSquareForZeroValues() {
        sideA = 0;
        sideB = 0;
        sideC = 0;

        triangle = new Triangle(triangleName, sideA, sideB, sideC);

    }
}