package com.softserve.edu.triangle;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

/**
 * Created by alin- on 09.12.2017.
 */
public class UserInput {

    private static List<Triangle> triangles = new ArrayList<>();

    static List<Triangle> startInput() {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.ENGLISH);
        while (true) {
            try {
                System.out.print("Input triangle params(Name, sideA, sideB, sideC): ");
                String input = scanner.nextLine();
                triangles.add(parseInputString(input));
                System.out.print("Do you want to add one more Triangle? If so, type y or yes: ");
                input = scanner.nextLine().trim();
                if (!(input.equalsIgnoreCase("y") || input.equalsIgnoreCase("yes"))) {
                    return triangles;
                }
            } catch (IllegalArgumentException e) {
                System.out.println("Triangle with given sides is not possible!");
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("You should type in 4 params: name, sideA, sideB, sideC. Please check your input!");
            }
        }
    }

    /*
        Parsing input string into triangle object
     */
    private static Triangle parseInputString(String s) throws ArrayIndexOutOfBoundsException {
        String[] triangleParams = s.split(",");
        String triangleName = triangleParams[0];
        double sideA = Double.parseDouble(triangleParams[1]);
        double sideB = Double.parseDouble(triangleParams[2]);
        double sideC = Double.parseDouble(triangleParams[3]);
        return new Triangle(triangleName, sideA, sideB, sideC);
    }


}
