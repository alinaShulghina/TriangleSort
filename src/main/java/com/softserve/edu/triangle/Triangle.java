package com.softserve.edu.triangle;

/**
 * Created by alin- on 09.12.2017.
 */
public class Triangle {

    private String name;
    private double square;

    public Triangle(String name, double sideA, double sideB, double sideC) {
        if (!isTriangleExists(sideA, sideB, sideC)) throw new IllegalArgumentException();
        this.name = name;
        this.square = calculateSquare(sideA, sideB, sideC);
    }

    public double getSquare() {
        return square;
    }

    public String getName() {
        return name;
    }

    /*
            Calculate square of triangle by Heron's formula
    */
    private double calculateSquare(double sideA, double sideB, double sideC) {
        double p = (sideA + sideB + sideC) / 2;
        return Math.sqrt(p * (p - sideA) * (p - sideB) * (p - sideC));
    }

    /*
        Check if triangle with given sides is possible
     */
    private boolean isTriangleExists(double sideA, double sideB, double sideC) {
        return ((sideA + sideB) > sideC) && ((sideB + sideC) > sideA) && ((sideA + sideC) > sideB);
    }

    @Override
    public String toString() {
        return "[" + name + "]: " +
                square + " sm";

    }
}
