package com.softserve.edu.triangle;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by alin- on 10.12.2017.
 */
public class Main {
    public static void main(String[] args){
        List<Triangle> triangles = UserInput.startInput();
        try {
            triangles = triangles.stream().sorted(Comparator.comparing(Triangle::getSquare)
                    .thenComparing(Triangle::getName)).collect(Collectors.toList());
        }catch (NullPointerException e){
            System.out.println("There is no triangles to sort!");
        }
        System.out.println("==================Triangle list:==================");
        triangles.forEach(System.out::println);
    }
}
